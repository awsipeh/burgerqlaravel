<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
      integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
      crossorigin="anonymous"
    />
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />
    <style>
      .container-admin{
        margin-top: 250px;
      }
      .menu{
        margin-top: 200px;
      }
      .btn-admin{
        margin-bottom: 30px
      }
    </style>
</head>
<body>
    <nav class="navbar">
    <div class="col mb-2">
    <a class="back" href="/Home"><svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-arrow-left-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: black;">
    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
    <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"></path>
  </svg></a>
  </div>
</nav>
    <div class="container-admin container text-center">
        <h1>ADMIN</h1>
        <br/>
        <button type="button" style="background-color: red;" class="btn-admin btn btn-lg btn-block text-light"><a href="/crud" class="menu text-decoration-none text-reset"><h3>Menu</h3></a></button>
    </div>
</body>
</html>