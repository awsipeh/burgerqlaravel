<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ADMIN</title>
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
        .search input[type=search] {
            width: 1000px
        }
        .arrow-back{
            position: absolute;
        }
        @media (max-width: 1366px) and (max-height: 768px){
            .search {
                transform: scale(0.7);
            }
            .search input[type=search] {
                width: 700px;
            }
        }
    </style>
</head>
<body>





        <!--navbar-->
            <nav class="navbar navbar-light" style="height: 90px;background-color: black;">
             <div class="col mb-5">
              <a class="navbar-brand" href="/admin">
               <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-arrow-left-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color:red;">
              <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
              <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"></path>
            </svg>
             </div>
              <h5 class="nav-item active col font-weight-bold" style="color: red;">DATA MENU</h5> 
              </a>  
           <div class="col mb-5">
             <button type="button" class="btn btn-bg float-right text-light" data-toggle="modal" data-target="#exampleModal" style="background-color: black;">
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color:red;">
                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"></path>
                </svg>
            </button>
           </div>
  </nav>
      
<div class="container-admin" style="margin: 10px">
    @if(session('success'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Succes!</strong> {{session('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

        <table class="table table-hover table-striped">
            <thead>
                <tr class="text-dark" style="background-color: red;">
                    <th>GAMBAR</th>
                    <th>NAMA MENU</th>
                    <th style="padding-left: 50px; padding-right: 50px; text-align: left;"></th>
                </tr>
            </thead>

            @foreach ($crud as $data)
            <tr>
                <td>{{$data->gambarMakanan}}</td>
                <td>{{$data->namaMakanan}}</td>               
                <td>
                    <a href="/crud/{{$data->id}}/edit" class="btn btn-sm text-light" style="background-color: black;">EDIT</a>
                    <a href="/crud/{{$data->id}}/delete" class="btn btn-sm" style="background-color: red;" onclick="return confirm('Yakin nih di hapus?')">DELETE</a>
                </td>
            </tr>
            @endforeach

        </table>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-light"  style="background: black;">
            <div class="modal-header" style="border-bottom: 2px solid white;">
                <h5 class="modal-title" id="exampleModalLabel" style="color: 
                red;">Tambah Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- Popup Forms --}}
                <form action="/crud/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Gambar</label>
                        <input style="border:none" name="gambarMakanan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Gambar" value="asset/">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Menu</label>
                        <input style="border:none" name="namaMakanan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Menu">
                    </div>
                </div>
                <div class="modal-footer" style="border-top: 2px solid white;">
                    <button type="button" class="btn text-light" style="background-color: red;" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn text-light" style="background-color: 
                    black;border-top: 2px solid white;">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>


