<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\menu;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search')){
            $crud = menu::where('nama', '%'.$request->search.'%')->get();
        } else {
            $crud = menu::all();
        }
        
        return view('crud.index', ['crud'=>$crud]);
        dd($crud);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
         $crud = menu::create([
            "gambarMakanan" => $request->gambarMakanan,
            "namaMakanan" => $request->namaMakanan,
        ]);


        return redirect('/crud')->with('success', 'Data Berhasil Diinput');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crud = menu::find($id);
        return view('/crud/edit', ['crud'=>$crud]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $crud = menu::where('id',$id)->update([
            "gambarMakanan" => $request->gambarMakanan,
            "namaMakanan" => $request->namaMakanan,
        ]);

        return redirect('/crud')->with('success', 'Data Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = menu::find($id);
        $crud->delete();

        return redirect('/crud')->with('success', 'Data Berhasil dihapus');
    }
}
