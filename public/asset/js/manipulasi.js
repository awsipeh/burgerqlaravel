
const menu = [
        {
          gambarMakanan: "asset/img/menu1.jpg",
          namaMakanan: "LIMITED TIME"          
        },{
          gambarMakanan: "asset/img/menu2.jpg",
          namaMakanan: "QUEEN OF THE WEEK"

        },{
          gambarMakanan: "asset/img/menu3.png",
          namaMakanan: "QUEEN SAVERS"

        },{
          gambarMakanan: "asset/img/menu4.jpg",
          namaMakanan: "QUEEN DEALS"

        },{
          gambarMakanan: "asset/img/menu5.jpg",
          namaMakanan: "BURGER BESAR"

        },{
          gambarMakanan: "asset/img/menu6.jpg",
          namaMakanan: "BURGER"
        },{
          gambarMakanan: "asset/img/menu7.jpg",
          namaMakanan: "FRIED CHIKEN"

        },{
          gambarMakanan: "asset/img/menu8.jpg",
          namaMakanan: "SNACK"

        },{
          gambarMakanan: "asset/img/menu9.jpg",
          namaMakanan: "BEVERAGES"

        },{
          gambarMakanan: "asset/img/menu10.png",
          namaMakanan: "MOZZARELLA WHOPPER MEAL"

        },{
          gambarMakanan: "asset/img/menu11.png",
          namaMakanan: "BBQ RASHER CHEESE WHOPPER MEAL"

        },{
          gambarMakanan: "asset/img/menu12.png",
          namaMakanan: "QUATTRO CHEESE WHOPPER MEAL"

        }
                ];


 const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
  }
  
 const callbackMap = (item, index)=>{
 const elmnt = document.querySelector(".makanan-menu"); 

        elmnt.innerHTML +=`
              <div class="container" style="margin-right:100px;">
                    <div class="row" style="float: left;">
                        <div class="col-sm m-2" >
                            <div class="card m-3" style="width: 18rem;">
                                <img src="${item.gambarMakanan}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h4 class="card-title">${item.namaMakanan}</h4>
            
                                    <a href="#" class="btn" style="background-color:orange; color: white;">Order</a>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>`; 
                }

  
menu.map(callbackMap);
jumlahMENU(menu);

  const buttonElement = document.querySelector('.button-keyword');

  buttonElement.addEventListener('click',()=>{
    const hasilPencarian = menu.filter((item, index)=>{
                              const inputElemnt = document.querySelector('.input-keyword');
                              const namaItem    = item.namaMakanan.toLowerCase();
                              const keyword     = inputElemnt.value.toLowerCase();


                              return namaItem.includes(keyword);
                        })

      document.querySelector('.makanan-menu').innerHTML = '';

      hasilPencarian.map(callbackMap);
      jumlahMENU(hasilPencarian);
});